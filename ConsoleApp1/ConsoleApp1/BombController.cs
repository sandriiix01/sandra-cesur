﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ConsoleApp1
{
    class Bomb
    {
        public class BombController : MonoBehaviour
        {
            void start()
            {
            }
            void Update()
            {
                Transform t = GetComponent<Transform>();
                Vector3 pos = t.position;
                pos.y = pos.y - 0.2f;
                t.position = pos;
            }
        }
    }
}
